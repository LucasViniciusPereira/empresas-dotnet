﻿using Prova.Net.Ioasys.EF.Context;
using System;

namespace Prova.Net.Ioasys.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IoasysContext _context;

        public UnitOfWork(IoasysContext context)
        {
            _context = context;
        }

        public bool Commit()
        {
            return _context.SaveChanges() > 0;
        }
    }
}
