﻿namespace Prova.Net.Ioasys.UoW
{
    public interface IUnitOfWork
    {
        bool Commit();
    }
}
