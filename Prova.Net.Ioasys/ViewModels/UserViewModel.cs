﻿using System.ComponentModel.DataAnnotations;

namespace Prova.Net.Ioasys.ViewModels
{
    public class UsuarioViewModel
    {
        public int UsuarioId { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "O e-mail informado não é válido.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "O campo senha deverá ser informado.")]
        [MaxLength(40, ErrorMessage = "A senha não poderá excerder o tamanho de 40 caracteres")]
        [MinLength(6, ErrorMessage = "A senha deverá conter pelo menos o tamanho de 6 caracteres")]
        public string Senha { get; set; }

        public string Nome { get; set; }
    }
}
