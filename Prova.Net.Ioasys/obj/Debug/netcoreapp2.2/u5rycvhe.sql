﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Empresa] (
    [EmpresaId] int NOT NULL IDENTITY,
    [RazaoSocial] varchar(200) NOT NULL,
    [NomeFantasia] varchar(200) NOT NULL,
    [TipoEmpresa] smallint NOT NULL,
    [CapitalSocial] decimal(10,2) NOT NULL,
    [CapitalAtribuido] decimal(10,2) NOT NULL,
    [CNPJ] varchar(14) NULL,
    [Email] varchar(100) NOT NULL,
    [CPF] varchar(11) NULL,
    CONSTRAINT [PK_Empresa] PRIMARY KEY ([EmpresaId])
);

GO

CREATE TABLE [Usuario] (
    [UsuarioId] int NOT NULL IDENTITY,
    [Nome] varchar(150) NOT NULL,
    [Senha] varchar(40) NOT NULL,
    [Email] varchar(100) NOT NULL,
    CONSTRAINT [PK_Usuario] PRIMARY KEY ([UsuarioId])
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190326113721_initial', N'2.2.3-servicing-35854');

GO

