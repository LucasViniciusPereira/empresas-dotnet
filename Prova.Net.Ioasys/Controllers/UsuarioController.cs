﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Prova.Net.Ioasys.Models;
using Prova.Net.Ioasys.Models.ValuesObjects;
using Prova.Net.Ioasys.Services.Interfaces;
using Prova.Net.Ioasys.ViewModels;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;

namespace Prova.Net.Ioasys.Controllers
{
    [Route("api/v1/users")]
    [ApiController]
    public class UsuarioController : Controller
    {
        private readonly SigningConfigurations _signingConfigurations;
        private readonly TokenConfigurations _tokenConfigurations;
        private readonly IUsuarioService _service;

        public UsuarioController(
            SigningConfigurations signingConfigurations, 
            TokenConfigurations tokenConfigurations,
            IUsuarioService service)
        {
            _signingConfigurations = signingConfigurations;
            _tokenConfigurations = tokenConfigurations;
            _service = service;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost]
        public IActionResult Post([FromBody]UsuarioViewModel model)
        {
            _service.CriarUsuario(new Usuario {
                Email = new Email(model.Email),
                Nome = model.Nome,
                Senha = model.Senha,
            });
        
            return Ok();
        }

        [HttpPost("/auth/sign_in")]
        [AllowAnonymous]
        public IActionResult Login([FromBody]UsuarioViewModel model)
        {
            var usuario = _service.BuscarUsuarioAutenticado(model.Email, model.Senha);

            if (usuario == null)
                return NotFound();

            var identity = new ClaimsIdentity(
                    new GenericIdentity(usuario.UsuarioId.ToString(), "Login"),
                    new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, usuario.UsuarioId.ToString()),
                        new Claim(JwtRegisteredClaimNames.Email, usuario.Email.Value)
                    }
                );

            var dataCriacao = DateTime.Now;
            var dataExpiracao = dataCriacao + TimeSpan.FromSeconds(_tokenConfigurations.Seconds);

            var handler = new JwtSecurityTokenHandler();
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _tokenConfigurations.Issuer,
                Audience = _tokenConfigurations.Audience,
                SigningCredentials = _signingConfigurations.SigningCredentials,
                Subject = identity,
                NotBefore = dataCriacao,
                Expires = dataExpiracao
            });

            var token = handler.WriteToken(securityToken);

            return Ok(new
            {
                authenticated = true,
                created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                accessToken = token,
                message = "OK"
            });
        }
    }
}