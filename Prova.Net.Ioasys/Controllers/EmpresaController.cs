﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Prova.Net.Ioasys.Models;
using Prova.Net.Ioasys.Services.Interfaces;
using Prova.Net.Ioasys.UoW;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Prova.Net.Ioasys.Controllers
{
    [ApiController]
    [Route("api/v1/enterprises")]
    public class EmpresaController : Controller
    {
        private readonly IEmpresaService _service;
        private readonly IUnitOfWork _uow;
        public EmpresaController(IEmpresaService service, IUnitOfWork uow)
        {
            _service = service;
            _uow = uow;
        }

        [HttpGet("{id}")]
        [Authorize("Bearer")]
        public IActionResult Get(int id)
        {
            var result = _service.BuscarEmpresa(id);
            return Ok(result);
        }

        [HttpGet]
        [Authorize("Bearer")]
        public IActionResult Get(short enterprise_types, string name)
        {
            Expression<Func<Empresa, bool>> predicate = c => true;

            if (enterprise_types > 0)
                predicate = predicate.And(c => (short)c.TipoEmpresa == enterprise_types);

            if (string.IsNullOrEmpty(name))
                predicate = predicate.And(c => c.NomeFantasia == name);

            var result = _service.BuscarEmpresas(predicate);
            return Ok(result);
        }

        [HttpGet]
        [Authorize("Bearer")]
        public IActionResult GetAll()
        {
            var result = _service.BuscarTodasEmpresas();

            return Ok(result.ToList());
        }
    }
}