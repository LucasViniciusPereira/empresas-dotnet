﻿using Microsoft.EntityFrameworkCore;
using Prova.Net.Ioasys.EF.Context;
using Prova.Net.Ioasys.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Prova.Net.Ioasys.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly IoasysContext _context;
        private readonly DbSet<T> _dbSet;

        public Repository(IoasysContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public virtual void Add(T entity)
        {
            _dbSet.Add(entity);
        }

        public virtual void Update(T entity)
        {
            _dbSet.Update(entity);
        }

        public virtual IEnumerable<T> Get(Expression<Func<T, bool>> predicate)
        {
            return _dbSet.Where(predicate);
        }

        public virtual IQueryable<T> Get(bool trackingChanges = true)
        {
            return trackingChanges ? _dbSet : _dbSet.AsNoTracking();
        }

        public virtual void Remove(T entity)
        {
            _dbSet.Remove(entity);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0 ? true : false;
        }

        public virtual void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
