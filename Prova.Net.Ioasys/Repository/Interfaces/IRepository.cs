﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Prova.Net.Ioasys.Repository.Interfaces
{
    public interface IRepository<T> : IDisposable where T : class
    {
        void Add(T entity);
        void Update(T entity);
        void Remove(T entity);
        bool SaveChanges();

        IEnumerable<T> Get(Expression<Func<T, bool>> predicate);
        IQueryable<T> Get(bool trackingChanges = true);
    }
}
