﻿using Prova.Net.Ioasys.Models;
using Prova.Net.Ioasys.Repository;
using Prova.Net.Ioasys.Repository.Interfaces;
using Prova.Net.Ioasys.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Prova.Net.Ioasys.Services
{
    public class EmpresaService : IEmpresaService
    {
        private readonly IRepository<Empresa> _repo;
        public EmpresaService(Repository<Empresa> repo)
        {

            _repo = repo;
        }

        public Empresa BuscarEmpresa(int id)
        {
            var result = _repo.Get(c => c.EmpresaId == id).SingleOrDefault();
            return result;
        }

        public IEnumerable<Empresa> BuscarEmpresas(Expression<Func<Empresa, bool>> predicate)
        {
            var result = _repo.Get(predicate);
            return result;
        }

        public IEnumerable<Empresa> BuscarTodasEmpresas()
        {
            return _repo.Get(c => true);
        }
    }
}
