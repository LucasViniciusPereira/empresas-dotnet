﻿using Prova.Net.Ioasys.Models;
using Prova.Net.Ioasys.Repository;
using Prova.Net.Ioasys.Repository.Interfaces;
using Prova.Net.Ioasys.Services.Interfaces;
using System.Linq;

namespace Prova.Net.Ioasys.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IRepository<Usuario> _repo;
        public UsuarioService(IRepository<Usuario> repo)
        {   
            _repo = repo;
        }

        public Usuario BuscarUsuarioAutenticado(string email, string senha)
        {
            var result = _repo.Get(c => c.Email.Value == email && c.Senha == senha).SingleOrDefault();

            return result;
        }

        public void CriarUsuario(Usuario model)
        {
            if (!model.EhValido())
                return;

            _repo.Add(model);
            _repo.SaveChanges();
        }
    }
}
