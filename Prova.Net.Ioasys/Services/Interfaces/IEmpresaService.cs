﻿using Prova.Net.Ioasys.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Prova.Net.Ioasys.Services.Interfaces
{
    public interface IEmpresaService
    {
        Empresa BuscarEmpresa(int id);
        IEnumerable<Empresa> BuscarEmpresas(Expression<Func<Empresa, bool>> predicate);

        IEnumerable<Empresa> BuscarTodasEmpresas();
    }
}