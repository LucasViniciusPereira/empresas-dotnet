﻿using Prova.Net.Ioasys.Models;

namespace Prova.Net.Ioasys.Services.Interfaces
{
    public interface IUsuarioService
    {
        Usuario BuscarUsuarioAutenticado(string email, string senha);
        void CriarUsuario(Usuario model);
    }
}
