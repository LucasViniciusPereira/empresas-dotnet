﻿namespace Prova.Net.Ioasys.Enums
{
    public enum ETipoEmpresa : short
    {
        NAOATRIBUIDO = 0,
        LTDA = 1,
        EIRELI = 2,
        MEI = 3,
        //...Outras
    }
}
