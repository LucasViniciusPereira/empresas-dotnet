﻿namespace Prova.Net.Ioasys.Models.ValuesObjects
{
    public class CPF
    {
        public string Value { get; set; }
        public bool IsValid { get; set; } = false;

        public CPF()
        {

        }

        public CPF(string cpf)
        {
            Value = cpf;
            IsValid = Validate();
        }

        private bool Validate()
        {
            //Código para validar o cpf

            return true;
        }

    }
}
