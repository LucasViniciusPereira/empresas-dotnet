﻿namespace Prova.Net.Ioasys.Models.ValuesObjects
{
    public class Email
    {
        public string Value { get; set; }
        public bool IsValid { get; set; } = false;

        public Email()
        {

        }

        public Email(string email)
        {
            Value = email;
            IsValid = Validate();
        }
        private bool Validate()
        {
            //Código para validar o email
            return true;
        }
    }
}
