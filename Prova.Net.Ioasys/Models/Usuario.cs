﻿using Prova.Net.Ioasys.Models.ValuesObjects;

namespace Prova.Net.Ioasys.Models
{
    public class Usuario
    {
        public int UsuarioId { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }
        public Email Email { get; set; }

        public bool EhValido()
        {
            //Regras para validar o objeto
            return true;
        }
    }
}
