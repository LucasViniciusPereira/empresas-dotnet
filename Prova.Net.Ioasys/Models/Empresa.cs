﻿using Prova.Net.Ioasys.Enums;
using Prova.Net.Ioasys.Models.ValuesObjects;

namespace Prova.Net.Ioasys.Models
{
    public class Empresa
    {
        public int EmpresaId { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public ETipoEmpresa TipoEmpresa { get; set; }
        public decimal CapitalSocial { get; set; }
        public decimal CapitalAtribuido { get; set; }

        public virtual CNPJ CNPJ { get; private set; }
        public virtual Email Email { get; private set; }
        public virtual CPF CPF { get; private set; }
    }
}
