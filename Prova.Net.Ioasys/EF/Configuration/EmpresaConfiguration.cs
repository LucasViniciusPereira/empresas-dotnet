﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Prova.Net.Ioasys.Models;

namespace Prova.Net.Ioasys.EF.Configuration
{
    public class EmpresaConfiguration : IEntityTypeConfiguration<Empresa>
    {
        public void Configure(EntityTypeBuilder<Empresa> builder)
        {
            builder.ToTable("Empresa").HasKey(c => c.EmpresaId);

            builder.Property(c => c.NomeFantasia).HasMaxLength(200).IsRequired().HasColumnType("varchar(200)");
            builder.Property(c => c.RazaoSocial).HasMaxLength(200).IsRequired().HasColumnType("varchar(200)");
            builder.Property(c => c.CapitalAtribuido).HasColumnType("decimal(10,2)");
            builder.Property(c => c.CapitalSocial).IsRequired().HasColumnType("decimal(10,2)");

            //Value Objects mapping
            builder.OwnsOne(c => c.Email, x =>
            {
                x.Property(c => c.Value).HasColumnName("Email").IsRequired().HasColumnType("varchar(100)");
                x.Ignore(c => c.IsValid);
            });

            builder.OwnsOne(c => c.CNPJ, x =>
            {
                x.Property(c => c.Value).HasColumnName("CNPJ").HasColumnType("varchar(14)");
                x.Ignore(c => c.IsValid);
            });

            builder.OwnsOne(c => c.CPF, x =>
            {
                x.Property(c => c.Value).HasColumnName("CPF").HasColumnType("varchar(11)");
                x.Ignore(c => c.IsValid);
            });
        }
    }
}
