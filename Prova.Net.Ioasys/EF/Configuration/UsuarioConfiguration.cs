﻿using Microsoft.EntityFrameworkCore;
using Prova.Net.Ioasys.Models;

namespace Prova.Net.Ioasys.EF.Configuration
{
    public class UsuarioConfiguration : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable("Usuario").HasKey(c => c.UsuarioId);

            builder.Property(c => c.Nome).HasMaxLength(150).IsRequired().HasColumnType("varchar(150)");
            builder.Property(c => c.Senha).HasMaxLength(40).IsRequired().HasColumnType("varchar(40)");

            //Value Objects mapping
            builder.OwnsOne(c => c.Email, x =>
            {
                x.Property(c => c.Value).HasColumnName("Email").IsRequired().HasColumnType("varchar(100)");
                x.Ignore(c => c.IsValid);
            });
        }
    }
}
