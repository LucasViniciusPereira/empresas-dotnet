﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Prova.Net.Ioasys.EF.Context;
using Prova.Net.Ioasys.Repository;
using Prova.Net.Ioasys.Repository.Interfaces;
using Prova.Net.Ioasys.Services;
using Prova.Net.Ioasys.Services.Interfaces;
using Prova.Net.Ioasys.UoW;

namespace Prova.Net.Ioasys.IoC
{
    public class Bootstrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //Serviços de infra
            services.AddScoped<IoasysContext>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            
            //Serviços de dominio
            services.AddScoped<IUsuarioService, UsuarioService>();
            services.AddScoped<IEmpresaService, EmpresaService>();

            //Repositorios
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
        }
    }
}
